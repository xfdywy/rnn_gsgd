p=$(dirname $0)
cd $p/../../
mkdir -p outputrnn/ptbc
mkdir -p savedmodel/ptbc/gsgd-ec-sgd-1-0

lr=20
data=./data/pennchar
model=lstm
opt=sgd
ecopt=bp
inneropt=sgd
clip=0.25
epoch=300
num_hidden=1024
emsize=1024
num_layer=1
for i in {0..3..1}
do
	CUDA_VISIBLE_DEVICES=$i python rnn/main_language_model.py --lr ${lr} --clip ${clip} --num_hidden ${num_hidden} --emsize ${emsize} --cuda --model ${model} --opt ${opt} --epoch ${epoch} --batch_size 20 --num_layer ${num_layer} --decay set  --seed $i --data  ${data} --save savedmodel/ptbc/ptbc/gsgd-ec-sgd-1-0/${data}_${model}_${opt}_${lr}_${num_hidden}_${emsize}_${num_layer}_20_${epoch} --dropout 0.6 | tee outputrnn/ptbc/${data}_${model}_${opt}_${lr}_${num_hidden}_${emsize}_${num_layer}_20_${epoch}.txt_$i &
done
