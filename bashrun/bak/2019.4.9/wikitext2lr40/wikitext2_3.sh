p=$(dirname $0)
cd $p/../../
mkdir -p outputrnn/wikitext2

expid=3
lr=40
data=./data/wikitext-2
model=lstm
opt=sgd
ec_opt=bp
inner_opt=sgd
clip=0.25
epoch=300
num_hidden=650
emsize=650
num_layer=2
bptt=35
dropout=0.5

for i in {0..3..1}
do
    mkdir -p savedmodel/wikitext2/${opt}-${ec_opt}-${inner_opt}-${expid}-${i}
	CUDA_VISIBLE_DEVICES=$i python main_language_model.py \
--data  ${data} \
--model ${model} \
--emsize ${emsize} \
--num_hidden ${num_hidden} \
--num_layer ${num_layer} \
--clip ${clip} \
--lr ${lr} \
--opt ${opt} \
--ec_opt ${ec_opt} \
--inner_opt ${inner_opt} \
--epoch ${epoch} \
--bptt ${bptt}  \
--cuda \
--batch_size 20 \
--decay set  \
--seed $i \
--dropout ${dropout} \
--tied \
--save savedmodel/wikitext2/${opt}-${ec_opt}-${inner_opt}-${expid}-${i}/wikitext2_${model}_${opt}_${ec_opt}_${inner_opt}_${lr}_${num_hidden}_${emsize}_${num_layer}_20_${epoch}_${dropout}  \
 | tee outputrnn/wikitext2/wikitext2_${model}_${opt}_${ec_opt}_${inner_opt}_${lr}_${num_hidden}_${emsize}_${num_layer}_20_${epoch}_${dropout}.txt_$i &
done

wait




