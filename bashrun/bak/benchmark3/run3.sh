cd /philly/eu1/msrlabs/v-yuewng/project/my_gsgd/rnn
mkdir -p outputrnn/mnistlog



#1
for i in {0..3..1}
do
	CUDA_VISIBLE_DEVICES=$i python main_seq_mnist.py  --lr 0.1 --clip 1 --num_layer 3 --num_hidden 100 --input_size 8 --cuda --epoch 180 --opt ec  --model  rnnrelu  --seed 1 | tee outputrnn/mnistlog/rnnrelu_3_ec_0.1_8_180.txt_$i &
done
wait


#2
for i in {0..3..1}
do
	CUDA_VISIBLE_DEVICES=$i python main_seq_mnist.py  --lr 0.1 --clip 1 --num_layer 3 --num_hidden 100 --input_size 8 --cuda --epoch 180 --opt ec  --model  rnntanh  --seed 1 | tee outputrnn/mnistlog/rnntanh_3_ec_0.1_8_180.txt_$i &
done
wait


#3
for i in {0..3..1}
do
	CUDA_VISIBLE_DEVICES=$i python main_seq_mnist.py  --lr 0.1 --clip 1 --num_layer 3 --num_hidden 100 --input_size 8 --cuda --epoch 180 --opt ec  --model  lstm  --seed 1 | tee outputrnn/mnistlog/lstm_3_ec_0.1_8_180.txt_$i &
done
wait


#4
for i in {0..3..1}
do
	CUDA_VISIBLE_DEVICES=$i python main_seq_mnist.py  --lr 0.1 --clip 1 --num_layer 3 --num_hidden 100 --input_size 8 --cuda --epoch 180 --opt ec  --model  gru  --seed 1 | tee outputrnn/mnistlog/gru_3_ec_0.1_8_180.txt_$i &
done
wait



