cd /philly/eu1/msrlabs/v-yuewng/project/my_gsgd/rnn
mkdir -p outputrnn/mnistlog



#1
for i in {0..3..1}
do
	CUDA_VISIBLE_DEVICES=$i python main_seq_mnist.py  --lr 0.1 --clip 1 --num_layer 1 --num_hidden 100 --input_size 14 --cuda --epoch 180 --opt ec  --model  rnnrelu  --seed $i | tee outputrnn/mnistlog/rnnrelu_1_ec_0.1_14_180.txt_$i &
done
wait


#2
for i in {0..3..1}
do
	CUDA_VISIBLE_DEVICES=$i python main_seq_mnist.py  --lr 0.1 --clip 1 --num_layer 1 --num_hidden 100 --input_size 14 --cuda --epoch 180 --opt ec  --model  rnntanh  --seed $i | tee outputrnn/mnistlog/rnntanh_1_ec_0.1_14_180.txt_$i &
done
wait


##3
#for i in {0..3..1}
#do
#	CUDA_VISIBLE_DEVICES=$i python main_seq_mnist.py  --lr 0.1 --clip 1 --num_layer 1 --num_hidden 100 --input_size 14 --cuda --epoch 180 --opt ec  --model  lstm  --seed 1 | tee outputrnn/mnistlog/lstm_1_ec_0.1_14_180.txt_$i &
#done
#wait
#
#
##4
#for i in {0..3..1}
#do
#	CUDA_VISIBLE_DEVICES=$i python main_seq_mnist.py  --lr 0.1 --clip 1 --num_layer 1 --num_hidden 100 --input_size 14 --cuda --epoch 180 --opt ec  --model  gru  --seed 1 | tee outputrnn/mnistlog/gru_1_ec_0.1_14_180.txt_$i &
#done
#wait
#


