p=$(realpath $0)
cd ${p%%my_gsgd*}my_gsgd
mkdir -p outputrnn/mnistlog

lr=0.1
clip=1
num_layer=3
num_hidden=100
input_size=8
epoch=180
opt=ec
model=rnnrelu
decay=multistep
for i in {0..3..1}
do
	CUDA_VISIBLE_DEVICES=$i python rnn/main_seq_mnist.py  --lr ${lr} --clip ${clip} --num_layer ${num_layer} --num_hidden ${num_hidden} --input_size ${input_size}  --cuda --epoch ${epoch} --opt ${opt}  --model  ${model}   --decay ${decay} --seed $i | tee outputrnn/mnistlog/${model}_${num_layer}_${opt}_${lr}_${input_size}_${epoch}.txt_$i &
done


lr=0.1
clip=1
num_layer=3
num_hidden=100
input_size=8
epoch=180
opt=bp
model=rnnrelu
for i in {0..3..1}
do
	CUDA_VISIBLE_DEVICES=$i python rnn/main_seq_mnist.py  --lr ${lr} --clip ${clip} --num_layer ${num_layer} --num_hidden ${num_hidden} --input_size ${input_size}  --cuda --epoch ${epoch} --opt ${opt}  --model  ${model}  --decay ${decay}   --seed $i | tee outputrnn/mnistlog/${model}_${num_layer}_${opt}_${lr}_${input_size}_${epoch}.txt_$i &
done
wait


#3
#for i in {0..3..1}
#do
#	CUDA_VISIBLE_DEVICES=$i python main_seq_mnist.py  --lr 0.01 --clip 1 --num_layer 3 --num_hidden 100 --input_size 8 --cuda --epoch 180 --opt ec  --model  lstm  --seed 1 | tee outputrnn/mnistlog/lstm_3_ec_0.01_8_180.txt_$i &
#done
#wait
#
#
##4
#for i in {0..3..1}
#do
#	CUDA_VISIBLE_DEVICES=$i python main_seq_mnist.py  --lr 0.01 --clip 1 --num_layer 3 --num_hidden 100 --input_size 8 --cuda --epoch 180 --opt ec  --model  gru  --seed 1 | tee outputrnn/mnistlog/gru_3_ec_0.01_8_180.txt_$i &
#done
#wait
#
#
#
