cd /philly/$(basename /philly/*)/msrlabs/v-yuewng/project/my_gsgd
mkdir -p outputrnn/

lr=0.5
data=wikitext-2
model=rnnrelu
opt=ec
epoch=100
num_hidden=200
emsize=200
num_layer=2
for i in {0..3..1}
do
	CUDA_VISIBLE_DEVICES=$i python rnn/main_language_model.py --lr ${lr} --clip 0.5 --num_hidden ${num_hidden} --emsize ${emsize} --cuda --model ${model} --opt ${opt} --epoch ${epoch} --batch_size 20 --num_layer ${num_layer} --decay set  --seed $i --data rnn/data/${data}| tee outputrnn/${data}_${model}_${opt}_${lr}_${num_hidden}_${emsize}_${num_layer}_20_${epoch}.txt_$i &
done


lr=0.5
data=wikitext-2
model=rnnrelu
opt=bp
epoch=100
num_hidden=200
emsize=200
num_layer=2
for i in {0..3..1}
do
	CUDA_VISIBLE_DEVICES=$i python rnn/main_language_model.py --lr ${lr} --clip 0.5 --num_hidden ${num_hidden} --emsize ${emsize} --cuda --model ${model} --opt ${opt} --epoch ${epoch} --batch_size 20 --num_layer ${num_layer} --decay set  --seed $i --data rnn/data/${data}| tee outputrnn/${data}_${model}_${opt}_${lr}_${num_hidden}_${emsize}_${num_layer}_20_${epoch}.txt_$i &
done

wait


lr=0.5
data=penn
model=rnnrelu
opt=ec
epoch=100num_hidden=200
emsize=200

num_layer=2
for i in {0..3..1}
do
	CUDA_VISIBLE_DEVICES=$i python rnn/main_language_model.py --lr ${lr} --clip 0.5 --num_hidden ${num_hidden} --emsize ${emsize} --cuda --model ${model} --opt ${opt} --epoch ${epoch} --batch_size 20 --num_layer ${num_layer} --decay set  --seed $i --data rnn/data/${data}| tee outputrnn/${data}_${model}_${opt}_${lr}_${num_hidden}_${emsize}_${num_layer}_20_${epoch}.txt_$i &
done


lr=0.5
data=penn
model=rnnrelu
opt=bp
epoch=100
num_hidden=200
emsize=200
num_layer=2
for i in {0..3..1}
do
	CUDA_VISIBLE_DEVICES=$i python rnn/main_language_model.py --lr ${lr} --clip 0.5 --num_hidden ${num_hidden} --emsize ${emsize} --cuda --model ${model} --opt ${opt} --epoch ${epoch} --batch_size 20 --num_layer ${num_layer} --decay set  --seed $i --data rnn/data/${data}| tee outputrnn/${data}_${model}_${opt}_${lr}_${num_hidden}_${emsize}_${num_layer}_20_${epoch}.txt_$i &
done

wait