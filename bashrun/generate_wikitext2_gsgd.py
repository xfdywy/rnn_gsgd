import os
import itertools
import shutil
if os.path.exists('innerrunbash/wikitext2'):
    shutil.rmtree('innerrunbash/wikitext2')
os.mkdir('innerrunbash/wikitext2')
os.chdir('innerrunbash/wikitext2')


def generate(
        lr,
        model,
        opt,
        ec_opt,
        inner_opt,
        dropout,
        num_layer,
        emsize,
        num_hidden,
        setting,
        cuda,
        idx,
        epoch=200,
        clip=0.25,
        bptt=35,
        dir='../..',
        data='./data/penn',
        expid=409,
):

    templete = '''
    p=$(dirname $0)
    cd $p/{dir}/
    mkdir -p outputrnn/wikitext2 
    mkdir -p savedmodel/wikitext2/{setting}-wikitext2-{model}-{opt}-{ec_opt}-{inner_opt}-{lr}-{num_hidden}-{emsize}-{num_layer}-{dropout}-{expid}-{idx}
        CUDA_VISIBLE_DEVICES={cuda}  python main_language_model.py \
    --data  {data} \
    --model {model} \
    --emsize {emsize} \
    --num_hidden {num_hidden} \
    --num_layer {num_layer} \
    --clip {clip} \
    --lr {lr} \
    --opt {opt} \
    --ec_opt {ec_opt} \
    --inner_opt {inner_opt} \
    --epoch {epoch} \
    --bptt {bptt}  \
    --cuda \
    --batch_size 20 \
    --decay set  \
    --seed {cuda} \
    --dropout {dropout} \
    --save savedmodel/wikitext2/{setting}-wikitext2-{model}-{opt}-{ec_opt}-{inner_opt}-{lr}-{num_hidden}-{emsize}-{num_layer}-{dropout}-{expid}-{idx}/{setting}-wikitext2-{model}-{opt}-{ec_opt}-{inner_opt}-{lr}-{num_hidden}-{emsize}-{num_layer}-{dropout}-{expid}-{idx}-best.pt   \
     | tee outputrnn/wikitext2/{setting}-wikitext2-{model}-{opt}-{ec_opt}-{inner_opt}-{lr}-{num_hidden}-{emsize}-{num_layer}-{dropout}-{expid}-{idx}.txt_{idx} &
    '''
    return templete.format(
        dir=dir,
        setting=setting,
        model=model,
        opt=opt,
        inner_opt=inner_opt,
        ec_opt=ec_opt,
        lr=lr,
        num_hidden=num_hidden,
        emsize=emsize,
        num_layer=num_layer,
        dropout=dropout,
        expid=expid,
        idx=idx,
        cuda=cuda,
        data=data,
        clip=clip,
        epoch=epoch,
        bptt=bptt

    )


def name(config):
    res = []
    for keys, values in config.items():
        res.append(str(keys))
        if isinstance(values, list):
            values = '_'.join([str(x) for x in values])

        values = str(values).replace('-', '=')
        res.append(values)

    return ('-'.join(res))


def check_right(all_config):
    res = []

    for ii in all_config:
        lr = ii["lr"]
        model = ii["model"]
        opt = ii["opt"]
        ec_opt = ii["ec_opt"]
        inner_opt = ii["inner_opt"]
        dropout = ii["dropout"]
        num_layer = ii["num_layer"]
        emsize = ii["emsize"]
        num_hidden = ii["num_hidden"]
        idx = ii["idx"]

        if opt == 'sgd' and (inner_opt != 'sgd' or ec_opt != 'bp'
                              ):
            continue
        if opt == 'adam' and (inner_opt != 'adam' or ec_opt != 'bp'
                               ):
            continue
        elif opt == 'gsgd' and inner_opt != 'sgd':
            continue

        elif opt == 'gsgd_adam' and inner_opt != 'adam':
            continue
        # else:
        if emsize != num_hidden:
            continue
        if (emsize == 200 and dropout !=0.2) or (emsize == 650 and dropout !=0.5) or    (emsize == 1500 and dropout !=0.65) :
            continue
        res.append(ii)
    return res


def generate_config(**kwargs):
    all_keys = list(kwargs.keys())
    all_values = list(kwargs.values())
    all_res_keys = [x[:-1] for x in all_keys]
    res = itertools.product(*all_values)
    res = [dict(zip(all_res_keys, x)) for nx, x in enumerate(res)]
    # for nx, x in enumerate(res):
    #     x['setting'] = nx
    return (res)


#
lrs = [20]
models =  ['lstm' ]
opts = ['sgd', 'gsgd']
ec_opts = ['ec', 'bp']
inner_opts = ['sgd']
dropouts=[0.65, 0.5, 0.2]
num_layers=[2]
emsizes=[200,650,1500]
num_hiddens=[200,650,1500]
idxs = list(range(3))
cudas = [0]
# adam_betas = [
#     [0.9, 0.98],
#     [0.9, 0.97],
#     [0.9, 0.99],
#     [0.91, 0.98],
#     [0.91, 0.97],
#     [0.91, 0.99],
#     [0.89, 0.98],
#     [0.89, 0.97],
#     [0.89, 0.99],
# ]
#
# adam_epsilons = [
#     1e-8,
#     5e-8,
#     5e-9
#
# ]
#
# lrs = [0 ]

# adam_betas = [' '.join([str(y) for y in x]) for x in adam_betas]
# adam_epsilons = [str(x) for x in adam_epsilons]

config_pool = {
"lrs" 			  :     lrs       ,
"models" 			  :     models         ,
"opts" 			  :     opts         ,
"ec_opts" 			  :     ec_opts         ,
"inner_opts" 		  :     inner_opts  ,
"dropouts"			  :     dropouts       ,
"num_layers"		  :     num_layers  ,
"emsizes"			  :     emsizes        ,
"num_hiddens"		  :     num_hiddens   ,
"idxs" 			  :     idxs         ,
"cudas"             : cudas
               }

all_config = generate_config(**config_pool)
all_res = check_right(all_config)
for nx,x in enumerate(all_res):
    x['setting'] = nx
# all_res = all_res


# for num, config in enumerate(all_res):
#     with open(str(num) + '-' + name(config) + '.sh', 'w') as fp:
#         fp.write(genreate(**config))


class bash_generator():
    def __init__(self):
        self.gpu_count = 0
        self.all_job_num = 0
        self.job_log = open('job_log.csv', 'w')
        self.job_num = 0

        self.set_config()
        self.init_bash()
        self.init_fp()
        self.init_joblog()

    def add_one(self, job):
        self.all_job_num += 1
        job_config_string = name(job)
        # print(self.gpu_count, job['cuda'])
        self.job_log.write(',{},{}\n'.format(self.all_job_num, job_config_string))
        job['cuda'] = self.gpu_count
        self.bash.append(generate(**job))
        # self.dir_tmp.append('{opt}-{ec_opt}-{inner_opt}-{eclayertype}-{setting}-{idx}'.format(opt=job['opt'],
        #                                                                       ec_opt=job['ec_opt'],
        #                                                                       inner_opt=job['inner_opt'],
        #                                                                       setting=job['setting'],
        #                                                                       idx=job['idx'],
        #                                                                     eclayertype=job['eclayertype']))

        self.gpu_count += 1

        if self.gpu_count == self.card_per_job:
            self.write_fp()
            self.init_joblog()
            self.init_bash()
            self.init_fp()
            self.gpu_count = 0

    def set_config(self, card_per_job=4):
        # self.game_num = game_num
        # self.job_num = job_num
        self.card_per_job = card_per_job
        # self.job_per_gpu = job_per_gpu
        # self.game_blok = card_per_job * job_per_gpu
        # self.game_per_job = self.game_num / self.job_num
        # self.init_bash()

    def init_bash(self):
        self.bash = []
        self.dir_tmp = []
        # self.bash.append('cd /philly/eu2/pnrsy/v-yuewng/project/philly-dqn-baseline/dqn_baseline')
        # self.bash.append('nvidia-smi --loop=2 &')

    def init_fp(self):
        self.fp = open('%d_job_%d.sh' % (self.job_num, self.all_job_num), 'w')
        self.job_num += 1

    def init_joblog(self):
        self.job_log.write('job_%d, \n' % (self.all_job_num))

    def write_fp(self):
        self.fp.write('\n'.join(self.bash))
        self.fp.write('\n wait')
        # self.fp.write(
            # '\n python eval_bleu_bash.py --evalres ./bleures/19.4.7-adam-lrbetaeps_1-bleu-res-{}.txt  --dir {} '.format(
            #     self.job_num, ' '.join(self.dir_tmp)))
        self.fp.close()

    def close_joblog(self):
        self.job_log.close()


a = bash_generator()
[a.add_one(x) for x in all_res]
a.write_fp()
a.close_joblog()
