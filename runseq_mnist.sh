python main_seq_mnist.py  --lr 0.1 --clip 1 --num_layer 3 --num_hidden 100 --input_size 28 --cuda --epoch 200 --opt ec  --model  rnnrelu
python main_seq_mnist.py  --lr 0.1 --clip 1 --num_layer 3 --num_hidden 100 --input_size 28 --cuda --epoch 200 --opt ec  --model  rnntanh
python main_seq_mnist.py  --lr 0.1 --clip 1 --num_layer 3 --num_hidden 100 --input_size 28 --cuda --epoch 200 --opt ec  --model  lstm
python main_seq_mnist.py  --lr 0.1 --clip 1 --num_layer 3 --num_hidden 100 --input_size 28 --cuda --epoch 200 --opt ec  --model  gru
