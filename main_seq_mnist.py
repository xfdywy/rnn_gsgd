import argparse
import time
import math
import os
import torch
import torch.nn as nn
# import torch.onnx
import torch.backends.cudnn as cudnn

import numpy as np
import tqdm
# import data
# from model.ptb import  RNNModel
from model.seq_mnist import LSTMModel
from model.seq_mnist import RNNModel_relu
from model.seq_mnist import RNNModel_tanh
from model.seq_mnist import GRUModel
from arguments import get_args
from data_loader.data_sequential_mnist import sequential_MNIST
from opt.gopt_general_rnn import gSGD


args = get_args()


if args.seed  == -1000:
    args.seed = int(time.time() * 1000)
torch.manual_seed(args.seed)
np.random.seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)

    torch.backends.cudnn.deterministic=True
    torch.cuda.manual_seed_all(args.seed)

args.device = torch.device("cuda" if args.cuda else "cpu")

print(args)

seq_len = int(784/args.input_size)

if args.model=='rnntanh':
    Model = RNNModel_tanh
elif args.model == 'rnnrelu':
    Model = RNNModel_relu
elif args.model =='lstm':
    Model = LSTMModel
elif args.model == 'gru':
    Model = GRUModel


model = Model(input_size=args.input_size, hidden_size=args.num_hidden, num_layers=args.num_layer, num_classes=args.num_class)
model.to(args.device)
# print(args.outdim, args.hidden, args.outdim, args.layernum)

training_data , testing_data = sequential_MNIST(args.batch_size, args.cuda)




criterion = nn.CrossEntropyLoss().to(args.device)
# cudnn.benchmark = True
all_batch_number_train = len(training_data)
all_batch_number_test = len(testing_data)

all_data_number_train = len(training_data.dataset)
all_data_number_test = len(testing_data.dataset)


optimizer = gSGD(models= model, lr = args.lr, args=args)
# optimizer = torch.optim.SGD(model.parameters(), lr = args.lr, momentum=args.momentum )
# optimizer = torch.optim.SGD(model.parameters(), lr=args.lr)


# scheduler = torch.optim.lr_scheduler.StepLR(optimizer , step_size=10, gamma =0.5)
def train(epoch):
    model.train()
    loss_interval = 0
    train_loss = 0
    correct = 0

    # for current_batch, (data, label) in enumerate(tqdm.tqdm(training_data, ncols = 0)):
    # scheduler.step()
    for batch_idx, (data, label) in enumerate( training_data):

        data = data.reshape(-1, seq_len, args.input_size).to(args.device)

        label =   label.to(args.device)


        output = model(data)

        loss = criterion(output, label)
        # if epoch <2:
        #     print(loss.item())



        optimizer.zero_grad()
        loss.backward()

        torch.nn.utils.clip_grad_norm_(model.parameters(), args.clip)
        optimizer.step()

        pred = output.data.max(1, keepdim=True)[1]  # get the index of the max log-probability
        correct += pred.eq(label.data.view_as(pred)).cpu().sum()
        loss_interval += loss.item()
        train_loss += loss.item()

        if batch_idx % args.log_interval == 0 and batch_idx > 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), all_data_number_train,
                       100. * batch_idx / all_batch_number_train, loss_interval / args.log_interval))

            loss_interval = 0



    train_loss /= batch_idx + 1

    print('\n[Training] Epoch: {} Average loss: {}, Accuracy: {}/{} ({})\n'.format(
        epoch, train_loss, correct, all_data_number_train,
        float(correct) / all_data_number_train))



def test(epoch):
    model.eval()
    test_loss =0
    correct = 0
    total = 0

    # print(epoch, ' ## Testing  ...')

    for data, label in testing_data:
        total += label.size(0)
        data = data.reshape(-1, seq_len, args.input_size).to(args.device)
        label = label.to(args.device)



        output = model(data)
        loss = criterion(output, label)
        test_loss += loss.item() * label.shape[0]
        pred = output.data.max(1, keepdim=True)[1]  # get the index of the max log-probability
        correct += pred.eq(label.data.view_as(pred)).cpu().sum()
    test_loss /= all_data_number_test
    print('\n[Vaidation] Epoch: {} Average loss: {}, Accuracy: {}/{} ({})\n'.format(
        epoch, test_loss, correct, all_data_number_test,
        float(correct) / all_data_number_test))


        # _, predicted = torch.max(output.data, 1)

        # correct += (predicted == label.data).sum().item()

    # print(epoch, ' ## Testing accurracy ...      ' , str(100 * correct / total) + '%')


def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res


if __name__ == "__main__":
    # print(11111111)
    for epoch in range(1, args.epochs + 1):
        optimizer.lr_decay(decay=args.decay , epoch= epoch , max_epoch=args.epochs, decay_state={'step': args.decay_step , 'gamma': 0.1})
        # optimizer.lr_decay(args.decay, epoch - 1, args.epochs + 1,
        #                    {'power': args.pow, 'step': [], 'gamma': 0.1})
        train(epoch)
        test(epoch)