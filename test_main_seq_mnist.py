import argparse
import time
import math
import os
import torch
import torch.nn as nn
# import torch.onnx
import torch.backends.cudnn as cudnn

import numpy as np
import tqdm
# import data
# from model.ptb import  RNNModel
# from model.seq_mnist import LSTMModel as Model
from model.seq_mnist import RNNModel as Model
from arguments import get_args
from data_loader.data_sequential_mnist import sequential_MNIST
args = get_args()

if args.seed  == -1000:
    args.seed = int(time.time() * 1000)
torch.manual_seed(args.seed)
np.random.seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)

    torch.backends.cudnn.deterministic=True
    torch.cuda.manual_seed_all(args.seed)

args.device = torch.device("cuda" if args.cuda else "cpu")


model = Model(inputDim=3, hiddenNum=args.hidden, outputDim=args.outdim, layerNum=args.layernum)
model.to(args.device)
# print(args.outdim, args.hidden, args.outdim, args.layernum)

training_data , testing_data = sequential_MNIST(args.batch_size, args.cuda)




criterion = nn.CrossEntropyLoss().to(args.device)
# cudnn.benchmark = True
all_batch_number_train = len(training_data)
all_batch_number_test = len(testing_data)

optimizer = torch.optim.SGD(model.parameters(), lr = args.lr, momentum=args.momentum )

data = torch.FloatTensor(3 ,2,3)+1
label = torch.ones(3).long()

def train(epoch,data,label):
    model.train()

    for current_batch, (_, _) in enumerate(tqdm.tqdm(training_data, ncols = 100)):

        data, label = data.to(args.device), label.to(args.device)
        # print(data.shape, '!!!!' , label.shape)
        # assert 1==2


        output = model(data)
        # print(output.shape)

        loss = criterion(output, label)
        print(loss.item())

        if (current_batch + 1) % 100 == 0:
            print( 'Current Loss:', {loss.data.item()})

        optimizer.zero_grad()
        loss.backward()

        torch.nn.utils.clip_grad_norm_(model.parameters(), args.gradient_clipping_value)
        optimizer.step()

        # for p in model.parameters():
        #     print(p.grad.data.abs().sum())


def test(epoch):
    model.eval()

    correct = 0
    total = 0

    print('Testing accurracy...')

    for data, label in testing_data:
        total += label.size(0)
        data, label = data.to(args.device), label.to(args.device)



        output = model(data)
        _, predicted = torch.max(output.data, 1)

        correct += (predicted == label.data).sum().item()

    print(str(100 * correct / total) + '%')


if __name__ == "__main__":
    # print(11111111)
    for epoch in range(1, args.epochs + 1):
        train(epoch,data,label)
        test(epoch)