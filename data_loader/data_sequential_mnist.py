
import torch
from torchvision import datasets, transforms
import torchvision

def sequential_MNIST(batch_size, cuda=False, dataset_folder='../data/mnist'):
    kwargs = {'num_workers': 4, 'pin_memory': True} if cuda else {}
    # train_loader = [ [torch.FloatTensor(2, 784,1)  ,  torch.LongTensor(2)] ]
    # test_loader = train_loader
    train_dataset = torchvision.datasets.MNIST(root=dataset_folder,
                                               train=True,
                                               transform=transforms.ToTensor(),
                                               download=True)

    test_dataset = torchvision.datasets.MNIST(root=dataset_folder,
                                              train=False,
                                              transform=transforms.ToTensor())

    # Data loader
    train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                               batch_size=batch_size,
                                               shuffle=True)

    test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                              batch_size=batch_size,
                                              shuffle=False)

    # train_loader = torch.utils.data.DataLoader(
    #     datasets.MNIST(dataset_folder, train=True, download=True,
    #                    transform=transforms.Compose([
    #                        transforms.ToTensor(),
    #                        transforms.Normalize((0.1307,), (0.3081,)),
    #                        # transform to sequence
    #                        # transforms.Lambda(lambda x: x.view(-1, 1))
    #                    ])),
    #     batch_size=batch_size, shuffle=True, drop_last=True, **kwargs)
    #
    # test_loader = torch.utils.data.DataLoader(
    #     datasets.MNIST(dataset_folder, train=False, transform=transforms.Compose([
    #         transforms.ToTensor(),
    #         transforms.Normalize((0.1307,), (0.3081,)),
    #         # transform to sequence
    #         # transforms.Lambda(lambda x: x.view(-1, 1))
    #     ])),
    #     batch_size=batch_size, shuffle=False, **kwargs)

    return (train_loader, test_loader)