import torch
import torch.nn as nn
import torch.nn.functional as F

import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable


torch.manual_seed(1)

# 模型基类，主要是用于指定参数和cell类型
class BaseModel(nn.Module):

    def __init__(self, inputDim, hiddenNum, outputDim, layerNum, cell, activation='relu' , dropout = 0.0, device='cpu'):

        super(BaseModel, self).__init__()
        self.hiddenNum = hiddenNum
        self.inputDim = inputDim
        self.outputDim = outputDim
        self.layerNum = layerNum
        self.device = device
        if cell == "RNN":
            self.cell = nn.RNN(input_size=self.inputDim, hidden_size=self.hiddenNum,
                        num_layers=self.layerNum, dropout=dropout,
                         nonlinearity=activation, batch_first=True,)
        if cell == "LSTM":
            self.cell = nn.LSTM(input_size=self.inputDim, hidden_size=self.hiddenNum,
                               num_layers=self.layerNum, dropout=dropout,
                               batch_first=True, )
        if cell == "GRU":
            self.cell = nn.GRU(input_size=self.inputDim, hidden_size=self.hiddenNum,
                                num_layers=self.layerNum, dropout=dropout,
                                 batch_first=True, )

        print("cell type:", self.cell)
        # self.fc_ih = nn.Linear(self.hiddenNum, self.outputDim)
        self.fc = nn.Linear(self.hiddenNum, self.outputDim)


# 标准RNN模型

class RNNModel_tanh(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, num_classes):
        super(RNNModel_tanh, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.cell = nn.RNN(input_size, hidden_size, num_layers, batch_first=True, nonlinearity='tanh')
        self.fc = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        # Forward propagate Rnn
        out, _ = self.cell(x )  # out: tensor of shape (batch_size, seq_length, hidden_size)

        # Decode the hidden state of the last time step
        out = self.fc(out[:, -1, :])
        return out

class RNNModel_relu(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, num_classes):
        super(RNNModel_relu, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.rnn = nn.RNN(input_size, hidden_size, num_layers, batch_first=True, nonlinearity='relu',bias=False)
        self.fc = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        # Forward propagate Rnn
        out, _ = self.rnn(x )  # out: tensor of shape (batch_size, seq_length, hidden_size)

        # Decode the hidden state of the last time step
        out = self.fc(out[:, -1, :])
        return out


# LSTM模型
class LSTMModel(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, num_classes):
        super(LSTMModel, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.rnn = nn.LSTM(input_size, hidden_size, num_layers, batch_first=True)
        self.fc = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        # Forward propagate Rnn
        out, _ = self.rnn(x )  # out: tensor of shape (batch_size, seq_length, hidden_size)

        # Decode the hidden state of the last time step
        out = self.fc(out[:, -1, :])
        return out

# GRU模型
class GRUModel(BaseModel):

    def __init__(self, input_size, hidden_size, num_classes, num_layers):
        super(GRUModel, self).__init__(input_size, hidden_size, num_classes, num_layers, cell="GRU")

    def forward(self, x):

        rnnOutput, hn = self.cell(x)
        rnnOutput = rnnOutput[:, -1, :].squeeze()

        out = self.fc(rnnOutput)
        # out = F.log_softmax(out)

        return out